package com.example.test.Controllers;

import com.example.test.Models.People;
import com.example.test.Models.ResponseObject;
import com.example.test.Reponsitories.PeopleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/Peoples")
public class PeopleController {
    @Autowired
    private PeopleRepository repository;
    @GetMapping("")
    List<People> getAllPeoples(){
        return repository.findAll();
    }
    @GetMapping("/{id}")
    ResponseEntity<ResponseObject> finById(@PathVariable Long id){
        Optional<People> foundPeople = repository.findById(id);
        if (foundPeople.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok","Tìm thấy thành công", foundPeople)
            );
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject("False","Không tìm thấy id ="+id, null)
            );
        }
    }
    @PostMapping("/insert")
    ResponseEntity<ResponseObject> insertPeople(@RequestBody People newPeople){
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "Thêm thành công", repository.save(newPeople))
        );
    }
    @PutMapping("/{id}")
    ResponseEntity<ResponseObject> updatePeople(@RequestBody People newPeople, @PathVariable Long id) {
        People updatePeople =  repository.findById(id)
                .map(people -> {
                    people.setMaLop(newPeople.getMaLop());
                    people.setTen(newPeople.getTen());
                    people.setTuoi(newPeople.getTuoi());
                    people.setGioiTinh(newPeople.getGioiTinh());
                    people.setDiaChi(newPeople.getDiaChi());
                    people.setSoDT(newPeople.getSoDT());
                    people.setEmail(newPeople.getEmail());
                    people.setHanhKiem(newPeople.getHanhKiem());
                    people.setMaChucDanh(newPeople.getMaChucDanh());
                    return repository.save(people);
                }).orElseGet(()->{
                    newPeople.setId(id);
                    return  repository.save(newPeople);
                });
        return  ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "Update Thành công", updatePeople)
        );
    }
    @DeleteMapping("/{id}")
    ResponseEntity<ResponseObject> deletePeople(@PathVariable Long id){
        boolean exists = repository.existsById(id);
        if (exists){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("OK", "Xóa thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseObject("False", "Xóa lỗi","")
        );
    }
//    List<MonHoc> getAllProduct(){
//        return reponsitory.findAll();
//    }
}
