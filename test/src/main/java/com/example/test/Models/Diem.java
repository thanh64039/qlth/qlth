package com.example.test.Models;

public class Diem {
    private long maMon;
    private long id;
    private double diemTP;
    private double diemThi;
    private double diemTongKet;

    public Diem(long maMon, long id, double diemTP, double diemThi, double diemTongKet) {
        this.maMon = maMon;
        this.id = id;
        this.diemTP = diemTP;
        this.diemThi = diemThi;
        this.diemTongKet = diemTongKet;
        diemTongKet = (diemTP+diemThi*2)/3;
    }

    @Override
    public String toString() {
        return "Diem{" +
                "maMon=" + maMon +
                ", id=" + id +
                ", diemTP=" + diemTP +
                ", diemThi=" + diemThi +
                ", diemTongKet=" + diemTongKet +
                '}';
    }

    public long getMaMon() {
        return maMon;
    }

    public long getId() {
        return id;
    }

    public double getDiemTP() {
        return diemTP;
    }

    public double getDiemThi() {
        return diemThi;
    }

    public double getDiemTongKet() {
        return diemTongKet;
    }

    public void setMaMon(long maMon) {
        this.maMon = maMon;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDiemTP(double diemTP) {
        this.diemTP = diemTP;
    }

    public void setDiemThi(double diemThi) {
        this.diemThi = diemThi;
    }

    public void setDiemTongKet(double diemTongKet) {
        this.diemTongKet = diemTongKet;
    }
}
