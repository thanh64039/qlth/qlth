package com.example.test.Models;

public class Lop {
    private String maLop;
    private String tenLop;
    private int phongHoc;
    private long id;

    public Lop(String maLop, String tenLop, int phongHoc, long id) {
        this.maLop = maLop;
        this.tenLop = tenLop;
        this.phongHoc = phongHoc;
        this.id = id;
    }

    public String getMaLop() {
        return maLop;
    }

    public String getTenLop() {
        return tenLop;
    }

    public int getPhongHoc() {
        return phongHoc;
    }

    public long getId() {
        return id;
    }

    public void setMaLop(String maLop) {
        this.maLop = maLop;
    }

    public void setTenLop(String tenLop) {
        this.tenLop = tenLop;
    }

    public void setPhongHoc(int phongHoc) {
        this.phongHoc = phongHoc;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Lop{" +
                "maLop='" + maLop + '\'' +
                ", tenLop='" + tenLop + '\'' +
                ", phongHoc=" + phongHoc +
                ", id=" + id +
                '}';
    }
}
