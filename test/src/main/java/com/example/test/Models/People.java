package com.example.test.Models;

import jakarta.persistence.*;

@Entity
public class People {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String maLop;
    private String ten;
    private int tuoi;
    private String gioiTinh;
    private String diaChi;
    private String soDT;
    private String email;
    private String hanhKiem;
    private String maChucDanh;
    public People(){}
    public People( String maLop, String ten, int tuoi, String gioiTinh
            , String diaChi, String soDT, String email, String hanhKiem, String maChucDanh) {

        this.maLop = maLop;
        this.ten = ten;
        this.tuoi = tuoi;
        this.gioiTinh = gioiTinh;
        this.diaChi = diaChi;
        this.soDT = soDT;
        this.email = email;
        this.hanhKiem = hanhKiem;
        this.maChucDanh = maChucDanh;
    }

    public Long getId() {
        return id;
    }

    public String getMaLop() {
        return maLop;
    }

    public String getTen() {
        return ten;
    }

    public int getTuoi() {
        return tuoi;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public String getSoDT() {
        return soDT;
    }

    public String getEmail() {
        return email;
    }

    public String getHanhKiem() {
        return hanhKiem;
    }

    public String getMaChucDanh() {
        return maChucDanh;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setMaLop(String maLop) {
        this.maLop = maLop;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public void setSoDT(String soDT) {
        this.soDT = soDT;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setHanhKiem(String hanhKiem) {
        this.hanhKiem = hanhKiem;
    }

    public void setMaChucDanh(String bangDiem) {
        this.maChucDanh = maChucDanh;
    }

    @Override
    public String toString() {
        return "People{" +
                "id=" + id +
                ", maLop='" + maLop + '\'' +
                ", ten='" + ten + '\'' +
                ", tuoi=" + tuoi +
                ", gioiTinh='" + gioiTinh + '\'' +
                ", diaChi='" + diaChi + '\'' +
                ", soDT='" + soDT + '\'' +
                ", email='" + email + '\'' +
                ", hanhKiem='" + hanhKiem + '\'' +
                ", bangDiem='" + maChucDanh + '\'' +
                '}';
    }
}
