package com.example.test.Models;

public class ChucDanh {
    private String maChucDanh;
    private String tenChucDanh;

    public ChucDanh(String maChucDanh, String tenChucDanh) {
        this.maChucDanh = maChucDanh;
        this.tenChucDanh = tenChucDanh;
    }

    @Override
    public String toString() {
        return "ChucDanh{" +
                "maChucDanh='" + maChucDanh + '\'' +
                ", tenChucDanh='" + tenChucDanh + '\'' +
                '}';
    }

    public String getMaChucDanh() {
        return maChucDanh;
    }

    public String getTenChucDanh() {
        return tenChucDanh;
    }

    public void setMaChucDanh(String maChucDanh) {
        this.maChucDanh = maChucDanh;
    }

    public void setTenChucDanh(String tenChucDanh) {
        this.tenChucDanh = tenChucDanh;
    }
}
