package com.example.test.Models;

public class MonHoc {
    private long maMon;
    private String tenMon;
    private int thoiGianHoc;
    private String giaoVienPhuTrach;

    public MonHoc(Long maMon, String tenMon, int thoiGianHoc, String giaoVienPhuTrach) {
        this.maMon = maMon;
        this.tenMon = tenMon;
        this.thoiGianHoc = thoiGianHoc;
        this.giaoVienPhuTrach = giaoVienPhuTrach;
    }

    @Override
    public String toString() {
        return "MonHoc{" +
                "maMon='" + maMon + '\'' +
                ", tenMon='" + tenMon + '\'' +
                ", thoiGianHoc=" + thoiGianHoc +
                ", giaoVienPhuTrach='" + giaoVienPhuTrach + '\'' +
                '}';
    }

    public long getMaMon() {
        return maMon;
    }

    public String getTenMon() {
        return tenMon;
    }

    public int getThoiGianHoc() {
        return thoiGianHoc;
    }

    public String getGiaoVienPhuTrach() {
        return giaoVienPhuTrach;
    }

    public void setMaMon(long maMon) {
        this.maMon = maMon;
    }

    public void setTenMon(String tenMon) {
        this.tenMon = tenMon;
    }

    public void setThoiGianHoc(int thoiGianHoc) {
        this.thoiGianHoc = thoiGianHoc;
    }

    public void setGiaoVienPhuTrach(String giaoVienPhuTrach) {
        this.giaoVienPhuTrach = giaoVienPhuTrach;
    }
}
