package com.example.test.Database;


import com.example.test.Models.People;
import com.example.test.Reponsitories.PeopleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PeopleDatabase {
    private static final Logger logger= LoggerFactory.getLogger(PeopleDatabase.class);
    @Bean
    CommandLineRunner initDatabase(PeopleRepository peopleRepository){
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                People people1 =new com.example.test.Models.People( "10A1", "Nguyễn Văn Thành", 15, "Nam", "Hiệp Hòa", "0983.591.841", "thanh@gmail.com", "Tốt","");
                People people2 = new com.example.test.Models.People("10A3","Nguyễn Thu Hà",15,"Nữ","Hiệp Hòa","0987654321","ha@gmail.com","Khá","");
                logger.info("Insert data: " + peopleRepository.save(people1));
                logger.info("Insert data: " + peopleRepository.save(people2));
            }
        };

    }
}
